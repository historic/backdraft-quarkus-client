
package guru.jini.backdraft.client;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URI;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Collections;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import org.apache.http.HttpResponse;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.eclipse.microprofile.config.ConfigProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebSocket(maxTextMessageSize = 2 * 1024 * 1024)
@ApplicationScoped
public class WebSocketToBackdraft {

    private static final Logger log = LoggerFactory.getLogger(WebSocketToBackdraft.class);
    private Session session;
    private WebSocketClient client;
    // private static final String backdraftLocation = "wss://backdraft.jini.rocks/ws/";
    private static final String backdraftLocation = "wss://backdraft.jini.rocks/ws/";
    private boolean keepOpen = true;
    private boolean loggedConnected = false;
    private String serverName;

    void start(@Observes StartupEvent ev) {

        try {
            // Make a hostname prefix that wont change for the host/port but has enough salt to not clash if its a common hostname
            String mac = UUID.randomUUID().toString();
            for (NetworkInterface ni : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                byte[] adr = ni.getHardwareAddress();
                if (adr == null || adr.length != 6 || ni.isLoopback()) {
                    continue;
                }
                mac = String.format("%02X%02X%02X%02X%02X%02X",
                        adr[0], adr[1], adr[2], adr[3], adr[4], adr[5]);
                log.debug("Mac is [{}] for [{}]", mac, ni.getDisplayName());
                break;
            }
            String hostName = InetAddress.getLocalHost().getHostName();
            String key = mac + hostName + ConfigProvider.getConfig().getValue("quarkus.http.port", Integer.class);
            log.debug("Key for server name is [{}]", key);
            byte[] sha = sha256(key.getBytes());
            serverName = "bd" + Base64.getEncoder().encodeToString(sha).substring(0, 10).replace('+', 'a').replace('/', 'b').replace('=', 'c').toLowerCase();
        } catch (Exception e) {
            log.debug("Error calculating server name to use:", e);
            serverName = "bd" + getRandomString(10);
        }

        // TODO - try and get scheduling in Quarkus working
        Thread t = new Thread(() -> {
            while (keepOpen) {
                if (session == null || !session.isOpen()) {
                    connect();
                }
                try {
                    Thread.sleep(30000);
                } catch (Exception e) {
                }
            }
        });
        t.setName("backdraft-keepalive");
        t.setDaemon(true);
        t.start();

    }

    void shutdown(@Observes ShutdownEvent ev) {
        disconnect();
    }

    private boolean connect() {
        log.debug("Initialising connection to backdraft at {}{}", backdraftLocation, getServerName());
        client = new WebSocketClient();
        try {
            // TODO - Wait till Quarkus can increase max websocket message size from 64k
            // Until then we can only deal with pages/apis < 64k
            client.setMaxIdleTimeout(70000);
            client.start();
            URI routerUri = new URI(backdraftLocation + getServerName());
            ClientUpgradeRequest request = new ClientUpgradeRequest();
            client.connect(this, routerUri, request);
            return true;
        } catch (Throwable t) {
            log.warn("Error connecting to backdraft: {}", t.getMessage());
            return false;
        }
    }

    void disconnect() {
        try {
            keepOpen = false;
            session.disconnect();
            session.close();
        } catch (Exception e) {
            log.debug("Error disconnecting:", e);
        }
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        log.debug("Connection closed: [{}]:[{}]", statusCode, reason);
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        log.debug("Connected: [{}]", session);
        this.session = session;
        if (!loggedConnected) {
            loggedConnected = true;
            log.info("\n\nBackdraft: This Quarkus server is available on the public internet via https://{}.backdraft.jini.rocks/\n", getServerName());
        }
    }

    @OnWebSocketMessage
    public void onMessage(String msg) {
        try {
            log.debug("Got msg: [{}]", msg);
            final NormalisedClientRequest normRequest = new NormalisedClientRequest(msg);
            normRequest.sendToLocalhost(
                    (HttpResponse response) -> {
                        try {
                            log.debug("Got response to HTTP request id [{}]", normRequest.getRequestId());
                            NormalisedServerResponse normResponse = new NormalisedServerResponse(normRequest.getRequestId(), response);
                            log.debug("Sending normalised server response back to backdraft over websocket");
                            session.getRemote().sendStringByFuture(normResponse.getEncodedHttpResponse());
                        } catch (Exception e) {
                            log.debug("Error sending response over websocket to backdraft router:", e);
                        }
                    },
                    (Exception e) -> {
                        log.debug("Error sending http request to local server:", e);
                        session.getRemote().sendStringByFuture("1\n" + normRequest.getRequestId() + "\n500\n\n" + e.getMessage());
                    }
            );
        } catch (Exception e) {
            log.debug("Error normalising http request:", e);
        }
    }

    @OnWebSocketError
    public void onError(Throwable cause) {
        log.debug("WebSocket Error: [{}]", cause.getMessage());
    }

    private String getServerName() {
        return serverName;
    }

    private String getRandomString(int n) {
        String AlphaNumericString = "0123456789abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index = (int) (AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }
        return sb.toString();
    }

    private byte[] sha256(byte[] bytesToHash) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        return md.digest(bytesToHash);
    }
}
