/* Copyright (C) Jini Guru Technologies (Mauritius) Limited - Company No. : 154309  - All Rights Reserved
 * Unauthorized copying of this file or any of its contents, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package guru.jini.backdraft.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.function.Consumer;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.eclipse.microprofile.config.ConfigProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Paul Carter-Brown
 */
public class NormalisedClientRequest {

    private static final Logger log = LoggerFactory.getLogger(NormalisedClientRequest.class);

    private final BufferedReader reader;
    private String requestId;

    private static final CloseableHttpAsyncClient client;
    private static final String localServer;
    private static final RequestConfig requestConfig;

    static {
        localServer = "http://localhost:" + ConfigProvider.getConfig().getValue("quarkus.http.port", Integer.class);
        client = HttpAsyncClients.createDefault();
        client.start();
        requestConfig = RequestConfig.custom().setSocketTimeout(60000).setConnectTimeout(2000).build();
    }

    public NormalisedClientRequest(String encodedHttpRequest) throws IOException {
        reader = new BufferedReader(new StringReader(encodedHttpRequest));
    }

    public String getRequestId() {
        return requestId;
    }

    public void sendToLocalhost(Consumer<HttpResponse> processResponse, Consumer<Exception> processException) throws IOException {
        String version = reader.readLine();
        log.debug("Version is [{}]", version);
        requestId = reader.readLine();
        log.debug("RequestId is [{}]", requestId);
        if (version == null || requestId == null) {
            throw new IllegalArgumentException("Invalid encoded http request. No version or requestId found");
        }
        String bits[] = reader.readLine().split(" ", 2);
        if (bits.length != 2) {
            throw new IllegalArgumentException("Invalid client request. Method and path not found");
        }
        String method = bits[0];
        String pathAndQueryString = bits[1];
        log.debug("Method is [{}] and path is [{}]", method, pathAndQueryString);

        String localUrl = localServer + pathAndQueryString;

        RequestBuilder builder = RequestBuilder
                .create(method)
                .setUri(localUrl)
                .setConfig(requestConfig);

        String line = reader.readLine();
        while (line != null) {
            if (line.isEmpty()) {
                break;
            } else {
                String[] headerBits = line.split(":", 2);
                builder.addHeader(headerBits[0], headerBits.length == 2 ? headerBits[1] : "");
            }
            line = reader.readLine();
        }

        byte[] body = IOUtils.toByteArray(reader, "utf-8");
        if (body != null && body.length > 0) {
            builder.setEntity(new ByteArrayEntity(body));
        }

        log.debug("Sending HTTP [{}] request to [{}]", method, localUrl);
        client.execute(builder.build(), new FutureCallback<HttpResponse>() {
            @Override
            public void completed(HttpResponse t) {
                processResponse.accept(t);
            }

            @Override
            public void failed(Exception e) {
                processException.accept(e);
            }

            @Override
            public void cancelled() {
            }

        });

    }

}
