/* Copyright (C) Jini Guru Technologies (Mauritius) Limited - Company No. : 154309  - All Rights Reserved
 * Unauthorized copying of this file or any of its contents, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package guru.jini.backdraft.client;

import java.io.IOException;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HeaderIterator;
import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Paul Carter-Brown
 */
public class NormalisedServerResponse {

    private static final Logger log = LoggerFactory.getLogger(NormalisedServerResponse.class);

    private final HttpResponse response;
    private final String requestId;

    public NormalisedServerResponse(String requestId, HttpResponse response) {
        log.debug("RequestId is [{}]", requestId);
        this.response = response;
        this.requestId = requestId;
    }

    public String getEncodedHttpResponse() throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("1");
        sb.append('\n');
        sb.append(requestId);
        sb.append('\n');
        sb.append(response.getStatusLine().getStatusCode());
        sb.append('\n');
        HeaderIterator it = response.headerIterator();
        while (it.hasNext()) {
            Header header = it.nextHeader();
            String headerNameLc = header.getName().toLowerCase();
            switch (headerNameLc) {
                case "connection":
                case "transfer-encoding":
                case "content-length":
                    break;
                default:
                    sb.append(header.getName());
                    sb.append(':');
                    sb.append(header.getValue());
                    sb.append("\n");
            }
        }
        sb.append('\n');
        byte[] body = IOUtils.toByteArray(response.getEntity().getContent());
        if (body != null && body.length != 0) {
            sb.append(new String(body, "utf-8"));
        }
        return sb.toString();
    }

}
